EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:NUCLEO-L476RG
LIBS:chrzaszcz_lib
LIBS:w_connectors
LIBS:warsztaty
LIBS:ir2153
LIBS:irf7103
LIBS:linStab-(1-GND,Adj,2-Out,3-in)
LIBS:lm2596
LIBS:lm2621
LIBS:mbi6651
LIBS:MC34063
LIBS:mcp73831
LIBS:microsd-sdio
LIBS:microsd-sdio-cd
LIBS:ncp551
LIBS:ncp1450
LIBS:nrf24l01
LIBS:pam8403
LIBS:pam8610
LIBS:rfid-rc522-module
LIBS:rgb-led
LIBS:stm32
LIBS:tl494
LIBS:tsop4856
LIBS:usblc6-4
LIBS:lm2596_stepdown_voltageConverter
LIBS:ErDrag-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L NUCLEO-L476RG U1
U 1 1 5AF9EE51
P 3300 3400
F 0 "U1" H 2900 4550 50  0000 C CNN
F 1 "NUCLEO-L476RG" H 3150 2250 50  0000 C CNN
F 2 "stm_modules:NUCLEO-L476RG" H 3300 3400 50  0001 C CNN
F 3 "" H 3300 3400 50  0001 C CNN
	1    3300 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 2500 2400 2500
Wire Wire Line
	4050 4300 4250 4300
Wire Wire Line
	4050 4200 4250 4200
Wire Wire Line
	2550 4200 2350 4200
Wire Wire Line
	2550 4300 2350 4300
Wire Wire Line
	4050 4100 4250 4100
Wire Wire Line
	4050 4000 4250 4000
Wire Wire Line
	4050 3800 4250 3800
Wire Wire Line
	4050 3900 4250 3900
Wire Wire Line
	4050 2500 4250 2500
Wire Wire Line
	6300 3850 6100 3850
Wire Wire Line
	6100 3950 6300 3950
Connection ~ 6300 4050
Connection ~ 2550 3100
Connection ~ 2550 3200
Connection ~ 2550 3600
Connection ~ 2550 3700
Connection ~ 2550 3800
Connection ~ 2550 3900
Connection ~ 2550 4000
$Comp
L NUCLEO-L476RG U1
U 2 1 5AF9F063
P 7050 3450
F 0 "U1" H 6650 4600 50  0000 C CNN
F 1 "NUCLEO-L476RG" H 6900 2300 50  0000 C CNN
F 2 "stm_modules:NUCLEO-L476RG" H 7050 3450 50  0001 C CNN
F 3 "" H 7050 3450 50  0001 C CNN
	2    7050 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 4150 8000 4150
Wire Wire Line
	7800 3850 8000 3850
Wire Wire Line
	7800 3650 8000 3650
Wire Wire Line
	7800 2650 8050 2650
Wire Wire Line
	6300 3450 6100 3450
Wire Wire Line
	6300 3250 6100 3250
Wire Wire Line
	6300 3150 6100 3150
Wire Wire Line
	6300 3350 6100 3350
Wire Wire Line
	2550 3500 2350 3500
Text GLabel 8000 3850 2    60   Input ~ 0
TIM15_1_PWMIN
Text GLabel 8000 4150 2    60   Input ~ 0
ADC_IN13
Text GLabel 8000 3650 2    60   Input ~ 0
ADC_IN16
Text GLabel 8050 2650 2    60   Input ~ 0
TIM8_1_PWM
Text GLabel 6100 3150 0    60   Input ~ 0
ADC_IN11
Text GLabel 6100 3250 0    60   Input ~ 0
TIM17_1_PWM
Text GLabel 6100 3350 0    60   Input ~ 0
TIM4_1_PWM
Text GLabel 6100 3450 0    60   Input ~ 0
TIM8_2_PWM
Text GLabel 6100 3850 0    60   Input ~ 0
TIM3_1_ENC
Text GLabel 6100 3950 0    60   Input ~ 0
TIM3_2_ENC
Text GLabel 4250 2500 2    60   Input ~ 0
UART4_RX
Text GLabel 2400 2500 0    60   Input ~ 0
UART4_TX
Text GLabel 2350 3500 0    60   Input ~ 0
TIM4_2_PWM
Text GLabel 2350 4200 0    60   Input ~ 0
ADC_IN3
Text GLabel 2350 4300 0    60   Input ~ 0
ADC_IN4
Text GLabel 4250 3800 2    60   Input ~ 0
TIM2_1_ENC
Text GLabel 4250 3900 2    60   Input ~ 0
TIM2_2_ENC
Wire Wire Line
	7800 2550 8050 2550
Wire Wire Line
	6300 2550 6100 2550
Text GLabel 8050 2550 2    60   Input ~ 0
TIM8_3_PWM
Text GLabel 6100 2550 0    60   Input ~ 0
TIM8_4_PWM
Text GLabel 4250 4300 2    60   Input ~ 0
ADC_IN1
Text GLabel 4250 4200 2    60   Input ~ 0
ADC_IN2
Text GLabel 4250 4100 2    60   Input ~ 0
ADC_IN15
Text GLabel 4250 4000 2    60   Input ~ 0
ADC_IN9
$Sheet
S 9550 2100 1350 600 
U 5AFD4634
F0 "KTIRY" 60
F1 "KTIRY.sch" 60
$EndSheet
$Comp
L Conn_01x03 J1
U 1 1 5AFD6DAE
P 3950 5300
F 0 "J1" H 3950 5500 50  0000 C CNN
F 1 "Conn_01x03" H 3950 5100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 3950 5300 50  0001 C CNN
F 3 "" H 3950 5300 50  0001 C CNN
	1    3950 5300
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03 J2
U 1 1 5AFD6E24
P 3950 5800
F 0 "J2" H 3950 6000 50  0000 C CNN
F 1 "Conn_01x03" H 3950 5600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 3950 5800 50  0001 C CNN
F 3 "" H 3950 5800 50  0001 C CNN
	1    3950 5800
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03 J3
U 1 1 5AFD6E48
P 3950 6300
F 0 "J3" H 3950 6500 50  0000 C CNN
F 1 "Conn_01x03" H 3950 6100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 3950 6300 50  0001 C CNN
F 3 "" H 3950 6300 50  0001 C CNN
	1    3950 6300
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03 J4
U 1 1 5AFD6E71
P 3950 6800
F 0 "J4" H 3950 7000 50  0000 C CNN
F 1 "Conn_01x03" H 3950 6600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 3950 6800 50  0001 C CNN
F 3 "" H 3950 6800 50  0001 C CNN
	1    3950 6800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 5AFD6ECB
P 3750 5400
F 0 "#PWR01" H 3750 5150 50  0001 C CNN
F 1 "GND" H 3750 5250 50  0000 C CNN
F 2 "" H 3750 5400 50  0001 C CNN
F 3 "" H 3750 5400 50  0001 C CNN
	1    3750 5400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5AFD6EFB
P 3750 5900
F 0 "#PWR02" H 3750 5650 50  0001 C CNN
F 1 "GND" H 3750 5750 50  0000 C CNN
F 2 "" H 3750 5900 50  0001 C CNN
F 3 "" H 3750 5900 50  0001 C CNN
	1    3750 5900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5AFD6F18
P 3750 6400
F 0 "#PWR03" H 3750 6150 50  0001 C CNN
F 1 "GND" H 3750 6250 50  0000 C CNN
F 2 "" H 3750 6400 50  0001 C CNN
F 3 "" H 3750 6400 50  0001 C CNN
	1    3750 6400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5AFD6F6B
P 3750 6900
F 0 "#PWR04" H 3750 6650 50  0001 C CNN
F 1 "GND" H 3750 6750 50  0000 C CNN
F 2 "" H 3750 6900 50  0001 C CNN
F 3 "" H 3750 6900 50  0001 C CNN
	1    3750 6900
	1    0    0    -1  
$EndComp
$Sheet
S 9550 2950 1350 600 
U 5AFD70B3
F0 "STEPDOWN" 60
F1 "STEPDOWN.sch" 60
$EndSheet
Text GLabel 3750 5200 0    60   Input ~ 0
TIM8_1_PWM
Text GLabel 3750 5700 0    60   Input ~ 0
TIM8_2_PWM
Text GLabel 3750 6200 0    60   Input ~ 0
TIM8_3_PWM
Text GLabel 3750 6700 0    60   Input ~ 0
TIM8_4_PWM
$Comp
L Conn_01x03 J5
U 1 1 5AFD81F6
P 4850 5300
F 0 "J5" H 4850 5500 50  0000 C CNN
F 1 "Conn_01x03" H 4850 5100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 4850 5300 50  0001 C CNN
F 3 "" H 4850 5300 50  0001 C CNN
	1    4850 5300
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03 J6
U 1 1 5AFD822F
P 4850 5800
F 0 "J6" H 4850 6000 50  0000 C CNN
F 1 "Conn_01x03" H 4850 5600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 4850 5800 50  0001 C CNN
F 3 "" H 4850 5800 50  0001 C CNN
	1    4850 5800
	1    0    0    -1  
$EndComp
Text GLabel 4650 5200 0    60   Input ~ 0
TIM4_1_PWM
Text GLabel 4650 5700 0    60   Input ~ 0
TIM4_2_PWM
$Comp
L GND #PWR05
U 1 1 5AFD8301
P 4650 5400
F 0 "#PWR05" H 4650 5150 50  0001 C CNN
F 1 "GND" H 4650 5250 50  0000 C CNN
F 2 "" H 4650 5400 50  0001 C CNN
F 3 "" H 4650 5400 50  0001 C CNN
	1    4650 5400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 5AFD8334
P 4650 5900
F 0 "#PWR06" H 4650 5650 50  0001 C CNN
F 1 "GND" H 4650 5750 50  0000 C CNN
F 2 "" H 4650 5900 50  0001 C CNN
F 3 "" H 4650 5900 50  0001 C CNN
	1    4650 5900
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR07
U 1 1 5AFD8351
P 4550 5000
F 0 "#PWR07" H 4550 4850 50  0001 C CNN
F 1 "+5V" H 4550 5140 50  0000 C CNN
F 2 "" H 4550 5000 50  0001 C CNN
F 3 "" H 4550 5000 50  0001 C CNN
	1    4550 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 5000 4550 5800
Wire Wire Line
	4550 5300 4650 5300
Wire Wire Line
	4550 5800 4650 5800
Connection ~ 4550 5300
$Comp
L Conn_01x03 J7
U 1 1 5AFD8410
P 5200 6800
F 0 "J7" H 5200 7000 50  0000 C CNN
F 1 "Conn_01x03" H 5200 6600 50  0000 C CNN
F 2 "Connectors_Molex:Molex_KK-6410-03_03x2.54mm_Straight" H 5200 6800 50  0001 C CNN
F 3 "" H 5200 6800 50  0001 C CNN
	1    5200 6800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 5AFD8473
P 5000 6900
F 0 "#PWR08" H 5000 6650 50  0001 C CNN
F 1 "GND" H 5000 6750 50  0000 C CNN
F 2 "" H 5000 6900 50  0001 C CNN
F 3 "" H 5000 6900 50  0001 C CNN
	1    5000 6900
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR09
U 1 1 5AFD84BE
P 4900 6450
F 0 "#PWR09" H 4900 6300 50  0001 C CNN
F 1 "+5V" H 4900 6590 50  0000 C CNN
F 2 "" H 4900 6450 50  0001 C CNN
F 3 "" H 4900 6450 50  0001 C CNN
	1    4900 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 6800 4900 6800
Wire Wire Line
	4900 6800 4900 6450
Text GLabel 5000 6700 0    60   Input ~ 0
TIM15_1_PWMIN
$Comp
L Conn_01x03 J8
U 1 1 5AFD87D7
P 6400 5300
F 0 "J8" H 6400 5500 50  0000 C CNN
F 1 "Conn_01x03" H 6400 5100 50  0000 C CNN
F 2 "Connectors_Molex:Molex_KK-6410-03_03x2.54mm_Straight" H 6400 5300 50  0001 C CNN
F 3 "" H 6400 5300 50  0001 C CNN
	1    6400 5300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 5AFD884D
P 5950 5550
F 0 "#PWR010" H 5950 5300 50  0001 C CNN
F 1 "GND" H 5950 5400 50  0000 C CNN
F 2 "" H 5950 5550 50  0001 C CNN
F 3 "" H 5950 5550 50  0001 C CNN
	1    5950 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 5300 6200 5300
Wire Wire Line
	6200 5200 5950 5200
Text GLabel 6200 5400 0    60   Input ~ 0
ADC_IN13
Wire Wire Line
	5950 5300 5950 5550
Wire Wire Line
	4050 3400 4450 3400
Wire Wire Line
	4050 3500 4450 3500
Wire Wire Line
	4450 3500 4450 3400
$Comp
L GND #PWR011
U 1 1 5AFD921E
P 4450 3500
F 0 "#PWR011" H 4450 3250 50  0001 C CNN
F 1 "GND" H 4450 3350 50  0000 C CNN
F 2 "" H 4450 3500 50  0001 C CNN
F 3 "" H 4450 3500 50  0001 C CNN
	1    4450 3500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 5AFD9395
P 2400 3400
F 0 "#PWR012" H 2400 3150 50  0001 C CNN
F 1 "GND" H 2400 3250 50  0000 C CNN
F 2 "" H 2400 3400 50  0001 C CNN
F 3 "" H 2400 3400 50  0001 C CNN
	1    2400 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 3400 2550 3400
$Comp
L GND #PWR013
U 1 1 5AFD96B2
P 6100 2950
F 0 "#PWR013" H 6100 2700 50  0001 C CNN
F 1 "GND" H 6100 2800 50  0000 C CNN
F 2 "" H 6100 2950 50  0001 C CNN
F 3 "" H 6100 2950 50  0001 C CNN
	1    6100 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 2950 6100 2950
Wire Wire Line
	4050 2700 4200 2700
$Comp
L +5V #PWR014
U 1 1 5AFD9AA7
P 4200 2700
F 0 "#PWR014" H 4200 2550 50  0001 C CNN
F 1 "+5V" H 4200 2840 50  0000 C CNN
F 2 "" H 4200 2700 50  0001 C CNN
F 3 "" H 4200 2700 50  0001 C CNN
	1    4200 2700
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR015
U 1 1 5B02BDB1
P 5950 5200
F 0 "#PWR015" H 5950 5050 50  0001 C CNN
F 1 "+5V" H 5950 5340 50  0000 C CNN
F 2 "" H 5950 5200 50  0001 C CNN
F 3 "" H 5950 5200 50  0001 C CNN
	1    5950 5200
	1    0    0    -1  
$EndComp
NoConn ~ 3000 1200
$EndSCHEMATC
