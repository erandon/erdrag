EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:NUCLEO-L476RG
LIBS:chrzaszcz_lib
LIBS:w_connectors
LIBS:warsztaty
LIBS:ir2153
LIBS:irf7103
LIBS:linStab-(1-GND,Adj,2-Out,3-in)
LIBS:lm2596
LIBS:lm2621
LIBS:mbi6651
LIBS:MC34063
LIBS:mcp73831
LIBS:microsd-sdio
LIBS:microsd-sdio-cd
LIBS:ncp551
LIBS:ncp1450
LIBS:nrf24l01
LIBS:pam8403
LIBS:pam8610
LIBS:rfid-rc522-module
LIBS:rgb-led
LIBS:stm32
LIBS:tl494
LIBS:tsop4856
LIBS:usblc6-4
LIBS:lm2596_stepdown_voltageConverter
LIBS:ErDrag-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LM1117-3.3 U9
U 1 1 5AFD7226
P 3450 3550
F 0 "U9" H 3300 3675 50  0000 C CNN
F 1 "LM1117-3.3" H 3450 3675 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 3450 3550 50  0001 C CNN
F 3 "" H 3450 3550 50  0001 C CNN
	1    3450 3550
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR038
U 1 1 5AFD728B
P 2750 3500
F 0 "#PWR038" H 2750 3350 50  0001 C CNN
F 1 "+5V" H 2750 3640 50  0000 C CNN
F 2 "" H 2750 3500 50  0001 C CNN
F 3 "" H 2750 3500 50  0001 C CNN
	1    2750 3500
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR039
U 1 1 5AFD72B6
P 4150 3500
F 0 "#PWR039" H 4150 3350 50  0001 C CNN
F 1 "VCC" H 4150 3650 50  0000 C CNN
F 2 "" H 4150 3500 50  0001 C CNN
F 3 "" H 4150 3500 50  0001 C CNN
	1    4150 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3550 2750 3550
Wire Wire Line
	2750 3550 2750 3500
Wire Wire Line
	3750 3550 4150 3550
Wire Wire Line
	4150 3550 4150 3500
$Comp
L GND #PWR040
U 1 1 5AFD72DF
P 3450 3850
F 0 "#PWR040" H 3450 3600 50  0001 C CNN
F 1 "GND" H 3450 3700 50  0000 C CNN
F 2 "" H 3450 3850 50  0001 C CNN
F 3 "" H 3450 3850 50  0001 C CNN
	1    3450 3850
	1    0    0    -1  
$EndComp
Connection ~ 3450 3850
$Comp
L C C3
U 1 1 5AFD7308
P 3000 3700
F 0 "C3" H 3025 3800 50  0000 L CNN
F 1 "C" H 3025 3600 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3038 3550 50  0001 C CNN
F 3 "" H 3000 3700 50  0001 C CNN
	1    3000 3700
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 5AFD7330
P 3850 3700
F 0 "C4" H 3875 3800 50  0000 L CNN
F 1 "C" H 3875 3600 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3888 3550 50  0001 C CNN
F 3 "" H 3850 3700 50  0001 C CNN
	1    3850 3700
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J9
U 1 1 5AFD6A6D
P 1300 1600
F 0 "J9" H 1300 1700 50  0000 C CNN
F 1 "Conn_01x02" H 1300 1400 50  0000 C CNN
F 2 "Connectors_Phoenix:PhoenixContact_MC-G_02x5.08mm_Angled" H 1300 1600 50  0001 C CNN
F 3 "" H 1300 1600 50  0001 C CNN
	1    1300 1600
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR041
U 1 1 5AFD6F82
P 1650 1800
F 0 "#PWR041" H 1650 1550 50  0001 C CNN
F 1 "GND" H 1650 1650 50  0000 C CNN
F 2 "" H 1650 1800 50  0001 C CNN
F 3 "" H 1650 1800 50  0001 C CNN
	1    1650 1800
	1    0    0    -1  
$EndComp
$Comp
L IRF4905 Q1
U 1 1 5AFD71B5
P 1950 1600
F 0 "Q1" H 2200 1675 50  0000 L CNN
F 1 "IRF4905" H 2200 1600 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 2200 1525 50  0001 L CIN
F 3 "" H 1950 1600 50  0001 L CNN
	1    1950 1600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1500 1500 1750 1500
Wire Wire Line
	1950 1800 1500 1800
Wire Wire Line
	1500 1800 1500 1600
$Comp
L +12V #PWR042
U 1 1 5AFD7558
P 2350 1500
F 0 "#PWR042" H 2350 1350 50  0001 C CNN
F 1 "+12V" H 2350 1640 50  0000 C CNN
F 2 "" H 2350 1500 50  0001 C CNN
F 3 "" H 2350 1500 50  0001 C CNN
	1    2350 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 1500 2350 1500
Connection ~ 1650 1800
NoConn ~ 5100 2000
$Comp
L LM2596_MODULE U10
U 1 1 5B02B2FB
P 3550 1550
F 0 "U10" H 3300 1350 60  0000 C CNN
F 1 "LM2596_MODULE" H 3600 1800 60  0000 C CNN
F 2 "Zmorph:LM2596S-Module" H 3550 1550 60  0000 C CNN
F 3 "" H 3550 1550 60  0000 C CNN
	1    3550 1550
	1    0    0    -1  
$EndComp
$Comp
L +12V #PWR043
U 1 1 5B02B436
P 3050 1500
F 0 "#PWR043" H 3050 1350 50  0001 C CNN
F 1 "+12V" H 3050 1640 50  0000 C CNN
F 2 "" H 3050 1500 50  0001 C CNN
F 3 "" H 3050 1500 50  0001 C CNN
	1    3050 1500
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR044
U 1 1 5B02B551
P 4050 1500
F 0 "#PWR044" H 4050 1350 50  0001 C CNN
F 1 "+5V" H 4050 1640 50  0000 C CNN
F 2 "" H 4050 1500 50  0001 C CNN
F 3 "" H 4050 1500 50  0001 C CNN
	1    4050 1500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR045
U 1 1 5B02B5AC
P 3550 1900
F 0 "#PWR045" H 3550 1650 50  0001 C CNN
F 1 "GND" H 3550 1750 50  0000 C CNN
F 2 "" H 3550 1900 50  0001 C CNN
F 3 "" H 3550 1900 50  0001 C CNN
	1    3550 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 3850 3850 3850
Connection ~ 3850 3550
Connection ~ 3000 3550
$EndSCHEMATC
